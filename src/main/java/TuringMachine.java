import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TuringMachine {
    private List<String> tasks = new ArrayList<>();
    private List<String> dataFromDB = new ArrayList<>();


    String taskCommandFromDB;
    String taskDataFromDB;

    enum Commands {EXIT, JUMP, ECHO, INC, ADD, COPY, SET, TEST};

    enum CommandsWithTwoData {ADD, COPY, SET, TEST};

    public void execute() throws Exception {

        tasks = dataFromDB;

        for (int i = 0; i < tasks.size(); i++) {

            String task = tasks.get(i);
            String[] taskSplit = task.split(" ");
            String command = taskSplit[0];

            int x = 0;
            int y = 0;
            int z = 0;

            if (!isCommandWithTwoData(taskSplit[0]) && !command.equals("EXIT")) {
                if (command.equals("null")) {
                    throw new Exception("Program exits with an error, because execution navigates to a line without command. " + "Line with data without command: " + i);
                }
                x = Integer.parseInt(taskSplit[1]);
            } else if (isCommandWithTwoData(taskSplit[0])) {
                y = Integer.parseInt(taskSplit[1]);
                z = Integer.parseInt(taskSplit[2]);
            }

            switch (command) {
                case "ECHO":
                    echo(x);
                    break;
                case "INC":
                    inc(x);
                    break;
                case "TEST":
                    if (test(y, z) == false) {
                        i++;}
                    break;
                case "JUMP":
                    i = getValueStoredAtSpecifiedIndexINT(i) - 1;
                    break;
                case "EXIT":
                    System.out.println("The program was stopped by \"EXIT\"");
                    System.exit(1);
                case "ADD":
                    add(y, z);
                    break;
                case "COPY":
                    copy(y, z);
                    break;
                case "SET":
                    set(y, z);
                    break;
                default:
                    throw new Exception("Unknown command " + taskCommandFromDB);
            }
        }
    }

    private void set(int y, int z) {
        setValueStoredAtSpecifiedIndex(y, z);
    }

    private void copy(int y, int z) throws IOException {
        setValueStoredAtSpecifiedIndex(z, getValueStoredAtSpecifiedIndexINT(y));
    }

    private void add(int y, int z) throws IOException {
        setValueStoredAtSpecifiedIndex(y,
                getValueStoredAtSpecifiedIndexINT(z)
                        + getValueStoredAtSpecifiedIndexINT(y));
    }

    private boolean test(int y, int z) throws IOException {
        return getValueStoredAtSpecifiedIndexINT(y) > getValueStoredAtSpecifiedIndexINT(z);
    }


    private void inc(int x) throws IOException {
        int increasesValue = getValueStoredAtSpecifiedIndexINT(x) + 1;
        setValueStoredAtSpecifiedIndex(x, increasesValue);
    }


    private void echo(int x) throws IOException {
        System.out.println("Value stored at the specified index: " + getValueStoredAtSpecifiedIndexString(x));
    }

    private String getValueStoredAtSpecifiedIndexString(int x) throws IOException {

        String phrase = "";
        String task = tasks.get(x);
        String[] taskSplit = task.split(" ");

        if (taskSplit.length == 2) {
            return taskSplit[1];
        } else {
            for (int i = 1; i < taskSplit.length; i++) {
                phrase = phrase + " " + taskSplit[i];
            }
            return phrase;
        }
    }


    private int getValueStoredAtSpecifiedIndexINT(int x) throws IOException {

        String task = tasks.get(x);
        String[] taskSplit = task.split(" ");

        return Integer.parseInt(taskSplit[1]);
    }


    private static boolean isCommandPresent(String data) {
        try {
            Enum.valueOf(Commands.class, data);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    private static boolean isCommandWithTwoData(String data) {
        try {
            Enum.valueOf(CommandsWithTwoData.class, data);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }


    private void setValueStoredAtSpecifiedIndex(int index, int valueToSet) {
        String task = tasks.get(index);
        String[] taskSplit = task.split(" ");
        String newTask;

        if (taskSplit.length < 2) {
            newTask = String.valueOf(valueToSet);
        } else {
            String command = taskSplit[0];
            newTask = command + " " + valueToSet;
            tasks.set(index, newTask);
        }
        tasks.set(index, newTask);
    }

    public void takeDataFromMysql(int numberOfTask) {
        try (
                Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/turing_machine_tasks", "root", "MySQLpwd1!");
        ) {
            PreparedStatement statement = connection.prepareStatement("SELECT commandname, value from commands right JOIN instructions on commands.command_id = instructions.command_id where taskNumber=?");
            statement.setInt(1, numberOfTask);
            statement.execute();
            ResultSet selectionResult = statement.getResultSet();

            System.out.println("Task instructions from task № " + numberOfTask + "\n");

            for (int i = 0; selectionResult.next(); i++){
                taskCommandFromDB = selectionResult.getString(1);
                taskDataFromDB = selectionResult.getString(2);

                dataFromDB.add(i, taskCommandFromDB + " " + taskDataFromDB);
                System.out.println("SQL data inserted: " + taskCommandFromDB + " | " + taskDataFromDB);

            }

            System.out.println("\n" + "Result of the execution task № " + numberOfTask + "\n");

        } catch (SQLException e) {
            System.out.println("Error occurred during SQL connection: " + e.getMessage());
        }
    }

    }
