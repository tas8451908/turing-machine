import java.io.IOException;

public class Application {

    public static void main(String[] args) throws Exception {

        TuringMachine turingMachine = new TuringMachine();
        turingMachine.takeDataFromMysql(3);

        try {
            turingMachine.execute();
        } catch (Exception e){
            System.out.println("Exception occurred: " + e.getMessage());
        }

    }


}
